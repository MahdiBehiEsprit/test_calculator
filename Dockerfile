FROM node:16.13.0-alpine3.14
WORKDIR /app
COPY package*.json .
RUN npm install
COPY . .
RUN addgroup app && adduser -S -G app app
USER app
ENV API_URL=http://api.myapp.com/
EXPOSE 3000
CMD ["npm", "start"]
